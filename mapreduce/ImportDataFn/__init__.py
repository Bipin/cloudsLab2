# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

from azure.storage.blob import BlockBlobService
#NEEDS TO BE CHANGED BEFORE DEPLOYMENT
ACCOUNT_NAME = "temp"
ACCOUNT_KEY = "temp"
CONTAINER = "temp"

def main(name: str) -> list:
    block_blob_service = BlockBlobService(account_name=ACCOUNT_NAME, account_key=ACCOUNT_KEY)
    blob = block_blob_service.get_blob_to_text(CONTAINER, name)
    return blob.content.splitlines()
