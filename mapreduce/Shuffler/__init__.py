# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

def main(wordtuples: list) -> list:
    dict = {}
    for tuple in wordtuples:
        word = tuple[0]
        existing_arr = dict.get(word)
        arr = existing_arr if existing_arr else []
        arr.append(tuple[1])
        dict[word] = arr
    return dict
