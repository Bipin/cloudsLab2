
import azure.durable_functions as df
import itertools

# trying to enter files
def orchestrator_function(context: df.DurableOrchestrationContext):
    files = ["mrinput-1.txt", "mrinput-2.txt", "mrinput-3.txt", "mrinput-4.txt"]

# fire multiple ImportDataFn and collect list of lines from each file
    tasks = [ context.call_activity("ImportDataFn", file) for file in files ]
    file_contents = yield context.task_all(tasks)
    contents = list(itertools.chain.from_iterable(file_contents)) # combine the result
    
#calling mapper
    tasks = [ context.call_activity("Mapper", c) for c in contents ]
    mapped = yield context.task_all(tasks)

    combined_mapped = list(itertools.chain.from_iterable(mapped))

#calling shuffler
    shuffled = yield context.call_activity("Shuffler", combined_mapped_results)

#calling reducer
    tasks = [ context.call_activity("Reducer", r) for r in shuffled.items()]
    result= yield context.task_all(tasks)
    
    return result

main = df.Orchestrator.create(orchestrator_function)