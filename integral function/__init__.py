import logging
import azure.functions as func
import math
 
def integral(min,max,N):
    sum = 0
    dx = (max-min)/N
    for i in range(N):
        sum += abs(math.sin(min+dx*(i+0.5)))
    return sum*dx

 
def main(req: func.HttpRequest) -> func.HttpResponse:
   logging.info('Python HTTP trigger function processed a request.')
   min = req.route_params.get('min')
   max = req.route_params.get('max')
   Ns = [10,100,1000,10000,100000,1000000]
   result = ""
   for N in Ns:
        number = integral(float(min),float(max),N)
        result+= str(number)+" "

   return func.HttpResponse(result)
